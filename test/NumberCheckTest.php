<?php

declare(strict_types=1);

namespace ChallengeLinio\Test;

use ChallengeLinio\Application\NumberCheck;
use PHPUnit\Framework\TestCase;

final class NumberCheckTest extends TestCase
{
    private $itemInstance;

    public function setUp(): void
    {
        parent::setUp();

        $this->itemInstance = new NumberCheck;
    }

    public function tearDown(): void
    {
        $this->itemInstance = null;
    }

    /**
     * @dataProvider  providerMultipleOf3
     * @param int $index
     * @param string $message
     */
    public function testEqualValueMultipleOf3(int $index, string $message): void
    {
        $item = $this->itemInstance->__invoke($index);
        $this->assertSame($message, $item->getMessage());
    }

    /**
     * @dataProvider providerMultipleOf5
     * @param int $index
     * @param string $message
     */
    public function testEqualValueMultipleOf5(int $index, string $message): void
    {
        $item = $this->itemInstance->__invoke($index);
        $this->assertSame($message, $item->getMessage());
    }

    /**
     * @dataProvider providerMultipleOf15
     * @param int $index
     * @param string $message
     */
    public function testEqualValueMultipleOf15(int $index, string $message): void
    {
        $item = $this->itemInstance->__invoke($index);
        $this->assertSame($message, $item->getMessage());
    }

    /**
     * @dataProvider providerOtherCases
     * @param int $index
     * @param string $message
     */
    public function testEqualValueOtherCases(int $index, string $message): void
    {
        $item = $this->itemInstance->__invoke($index);
        $this->assertSame($message, $item->getMessage());
    }

    public function providerMultipleOf3(): array
    {
        return [
            "3" => [
                "index" => 3,
                "message" => "Linio",
            ],
            "6" => [
                "index" => 6,
                "message" => "Linio",
            ],
            "9" => [
                "index" => 9,
                "message" => "Linio",
            ],
            "12" => [
                "index" => 12,
                "message" => "Linio",
            ],
        ];
    }

    public function providerMultipleOf5(): array
    {
        return [
            "5" => [
                "index" => 5,
                "message" => "IT",
            ],
            "35" => [
                "index" => 35,
                "message" => "IT",
            ],
            "40" => [
                "index" => 40,
                "message" => "IT",
            ],
            "50" => [
                "index" => 50,
                "message" => "IT",
            ],
            "55" => [
                "index" => 55,
                "message" => "IT",
            ],
        ];
    }

    public function providerMultipleOf15(): array
    {
        return [
            "15" => [
                "index" => 15,
                "message" => "Linianos",
            ],
            "30" => [
                "index" => 30,
                "message" => "Linianos",
            ],
            "45" => [
                "index" => 45,
                "message" => "Linianos",
            ],
            "60" => [
                "index" => 60,
                "message" => "Linianos",
            ],
        ];
    }

    public function providerOtherCases(): array
    {
        return [
            "1" => [
                "index" => 1,
                "message" => "1",
            ],
            "2" => [
                "index" => 2,
                "message" => "2",
            ],
            "4" => [
                "index" => 4,
                "message" => "4",
            ],
            "7" => [
                "index" => 7,
                "message" => "7",
            ],
            "8" => [
                "index" => 8,
                "message" => "8",
            ],
            "11" => [
                "index" => 11,
                "message" => "11",
            ],
        ];
    }
}
