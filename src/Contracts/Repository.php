<?php

declare(strict_types=1);

namespace ChallengeLinio\Contracts;

interface Repository
{
    public function items(): array;
}