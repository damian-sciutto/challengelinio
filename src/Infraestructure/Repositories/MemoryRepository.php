<?php

declare(strict_types=1);

namespace ChallengeLinio\Infraestructure\Repositories;

use ChallengeLinio\Contracts\Repository;

class MemoryRepository implements Repository
{
    private const LIMIT_ITEMS = 100;

    private $result = [];

    public function __construct()
    {
        $this->result = [];
    }

    public function items(): array
    {
        for ($position = 1; $position <= self::LIMIT_ITEMS; $position++) {
            $this->result[] = $position;
        }

        return $this->result;
    }
}