<?php

declare(strict_types=1);

namespace ChallengeLinio\ValueObjects;

class Item
{
    protected $position;
    protected $message;

    public function setPosition(int $position) {
        $this->position = $position;
    }

    public function getPosition(): int {
        return $this->position;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

    public function getMessage(): string {
        return $this->message;
    }
}