<?php

declare(strict_types=1);

namespace ChallengeLinio\Application;

use ChallengeLinio\ValueObjects\Item;

final class NumberCheck
{
    protected const LINIO = "Linio";
    protected const IT = "IT";
    protected const LINIANOS = "Linianos";

    protected $item;

    public function __construct()
    {
        $this->item = new Item;
    }

    public function __invoke(int $index): Item
    {
        $this->item->setPosition($index);
        $this->item->setMessage((string) $index);

        $this->isMultiple($index, 3, self::LINIO);
        $this->isMultiple($index, 5, self::IT);
        $this->isMultiple($index, 15, self::LINIANOS);

        return $this->item;
    }

    private function isMultiple(int $index, int $multiple, string $message): void
    {
        if ($index % $multiple === 0) {
            $this->item->setPosition($index);
            $this->item->setMessage($message);
        }
    }
}