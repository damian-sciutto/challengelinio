<?php

declare(strict_types=1);

namespace ChallengeLinio\Application;

use ChallengeLinio\Contracts\Repository;

final class Challenge
{
    private $repository;
    private $collection;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
        $this->collection = [];
    }

    public function __invoke()
    {
        $items = $this->repository->items();

        foreach ($items as $item) {
            $numberCheck = new NumberCheck;

            $itemValue = $numberCheck($item);

            array_push($this->collection, [
                "position" => $itemValue->getPosition(),
                "message" => $itemValue->getMessage(),
            ]);
        }

        return $this->collection;
    }
}