<?php

declare(strict_types=1);

use ChallengeLinio\Infraestructure\Repositories\MemoryRepository;
use ChallengeLinio\Application\Challenge;

require_once __DIR__ . '/vendor/autoload.php';

$challenge = new Challenge(new MemoryRepository);

print json_encode($challenge(), JSON_PRETTY_PRINT);
